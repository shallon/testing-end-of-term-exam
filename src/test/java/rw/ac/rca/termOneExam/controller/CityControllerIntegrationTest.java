package rw.ac.rca.termOneExam.controller;

import org.checkerframework.checker.units.qual.C;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.dto.CreateCityDTO;

import java.util.Objects;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CityControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void getAll_testSuccess() {
        ResponseEntity<String> response = restTemplate.getForEntity("/api/cities/all", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void findById_testSuccess() {
        ResponseEntity<String> response = restTemplate.getForEntity("/api/cities/id/103", String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void findById_Failure() {
        ResponseEntity<String> response = restTemplate.getForEntity("/api/cities/id/1", String.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void create_testSuccess() {
        CreateCityDTO createCityDTO = new CreateCityDTO("Karongi", 30);
        ResponseEntity<CreateCityDTO> response = restTemplate.postForEntity("/api/cities/add", createCityDTO, CreateCityDTO.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }
    @Test
    public void saveProduct__Failure(){
        CreateCityDTO createCityDTO = new CreateCityDTO("Kigali", 24);
        ResponseEntity<City> responseEntity = restTemplate.postForEntity("/api/cities/add", createCityDTO, City.class);
        Assertions.assertEquals(400, responseEntity.getStatusCodeValue());

    }

}
